<?php

namespace JyPrint;

/**
 * Class Factory
 *
 * @method static \JyPrint\Feieyun\Application               Feieyun(array $config)
 * @method static \JyPrint\Ylyun\Application                 Ylyun(array $config)
 * @method static \JyPrint\Ushengyun\Application             Ushengyun(array $config)
 * @method static \JyPrint\Zhongwuyun\Application            Zhongwuyun(array $config)
 * @method static \JyPrint\Bjguntong\Application             Bjguntong(array $config)
 * @method static \JyPrint\Xpyun\Application                 Xpyun(array $config)
 * @method static \JyPrint\Poscom\Application                Poscom(array $config)
 * @method static \JyPrint\Spyun\Application                 Spyun(array $config)
 * @method static \JyPrint\Stzg\Application                  Stzg(array $config)
 * @method static \JyPrint\Stzg2\Application                 Stzg2(array $config)
 * @method static \JyPrint\Daqu\Application                  Daqu(array $config)
 * @method static \JyPrint\Daqu\Application                  Wending(array $config)
 * @method static \JyPrint\A365\Application                  A365(array $config)
 */
class JyPrint
{
    /**
     * @param string $name
     * @param array  $config
     *
     */
    public static function make($name, $config = [])
    {
        $namespace   = self::studly($name);
        $application = "\\JyPrint\\{$namespace}\\Application";
        return new $application($config);
    }
    
    /**
     * Dynamically pass methods to the application.
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        return self::make($name, ...$arguments);
    }
    
    public static function studly($value)
    {
        $value = ucwords(str_replace(['-', '_'], ' ', $value));
        return str_replace(' ', '', $value);
    }
}
