<?php

namespace JyPrint\Wending;

use JyUtils\Str\Str;
use JyPrint\Kernel\Http;
use JyPrint\Kernel\Response;

/**
 * 文鼎云
 */
class Application extends Response
{
    use BarCode;

    private $config;
    private $requestUrl = 'https://saas.excelsecu.com';
    protected $printName = '文鼎云';
    protected $errorNum = [
        '1011501' => '验证签名失败',
        '1011502' => '设备序列号或密钥不正确',
        '1011503' => '解析biz参数失败',
        '1011504' => '缺少业务参数',
        '1011505' => '数据写入失败',
        '1011506' => '重复添加打印机',
        '1011507' => '无效的打印机序列号',
        '1011508' => '小票打印失败',
        '1011509' => '无效的请求参数',
        '1011510' => '不支持的请求类型',
        '1011511' => '请求超时',
        '1011512' => '未处理的异常',
        '1011513' => '模板Base64解码失败',
        '1011514' => '无效的count参数',
        '1011515' => '打印内容过长',
        '1011516' => '设备未在线',
        '1011517' => '无效的printId',
        '1011518' => '播报内容不能超过32个字符',
        '1011519' => '无效的时间戳',
        '1011520' => '无效的APPID',
        '1011521' => '重复的请求ID',
        '101522'  => '业务类型不支持',
        '101523'  => '业务类型不匹配',
        '101524'  => 'MediaType不支持',
    ];

    public function __construct(array $config = [])
    {
        $this->config = $config;
        $this->setError('');
    }

    /**
     * 取公共参数
     *
     * @param array $params  业务参数
     * @param int   $biztype https://iot-doc.excelsecu.com/doc/v1/restful_api.html#%E4%B8%9A%E5%8A%A1%E7%B1%BB%E5%9E%8B
     *                       1=添加打印机，2=删除打印机，3=打印小票，4=清除队列，5=设备状态，6=打印状态
     * @return array
     */
    private function getCommonParam($params, $biztype)
    {
        $data         = [
            'appid'     => $this->config['appid'],
            'reqid'     => Str::uuid(''),
            'timestamp' => time() * 1000,
            'biztype'   => $biztype,
            'biz'       => json_encode($params, JSON_UNESCAPED_UNICODE),
        ];
        $str_to_sign  = sprintf("%s%d%s%d%s", $data['appid'], $data['timestamp'], $data['biz'], $biztype, $data['reqid']);
        $result       = hash_hmac("sha256", $str_to_sign, $this->config['secret'], true);
        $data['sign'] = base64_encode($result);
        return $data;
    }

    /**
     * 添加打印机
     *
     * @param array $prints 打印机列表
     * @return bool
     */
    public function add($prints)
    {
        $prints = $this->onlyfields($prints, ['sn', 'key', 'name', 'lang']);
        $params = $this->getCommonParam([
            [
                'sn'  => $prints['sn'],
                'key' => $prints['key'],
            ],
        ], 1);
        $res    = Http::httpPostJson($this->requestUrl . "/openapi/v1/iot/printer/add", $params);
        if ($res['code'] == 0) {
            return true;
        }
        return $this->setError($res);
    }

    /**
     * 删除打印机
     *
     * @param string $sn 打印机编号
     */
    public function del($sn)
    {
        $params = $this->getCommonParam([
            'sn' => $sn,
        ], 2);
        $res    = Http::httpPostJson($this->requestUrl . "/openapi/v1/iot/printer/remove", $params);
        if ($res['code'] == 0) {
            return true;
        }
        return $this->setError($res);
    }

    /**
     * 获取打印机状态
     * https://www.yuque.com/docs/share/3138de29-7cd0-44d5-bede-c7ccf8c1c811#6574e4f329bcc1ce18a1f7a98feb5a5e
     * - onlineStatus      设备联网状态: 0 当前离线 1 在线
     * - workStatusDesc    设备工作状态说明
     */
    public function getStatus($sn)
    {
        $params = $this->getCommonParam([
            'sn' => $sn,
        ], 5);
        $res    = Http::httpPostJson($this->requestUrl . "/openapi/v1/iot/printer/device-status", $params);
        if ($res['code'] == 0) {
            return intval($res['data']['status'] == 'online');
        }
        return $this->setError($res);
    }

    /**
     * 打印小票
     * https://www.yuque.com/docs/share/3138de29-7cd0-44d5-bede-c7ccf8c1c811#191fca168ed125f0c43a21b3958f46dd
     */
    public function printTicket($param)
    {
        $param = $this->onlyfields($param, ['sn', 'tts', 'content', 'number']);
        if (strpos($param['content'], '<printer>') === false) {
            $param['content'] = '<printer>' . $param['content'] . '</printer>';
        }
        // dd($param);
        $params = $this->getCommonParam([
            'sn'      => $param['sn'],
            'tts'     => $param['tts'] ?: '您有新订单，请及时处理。',
            'content' => base64_encode($param['content']),
            'count'   => $param['number'] ?: 1,
        ], 3);
        $res    = Http::httpPostJson($this->requestUrl . "/openapi/v1/iot/printer/print", $params);
        if ($res['code'] == 0) {
            return $res['data']['printId'] ?? true;
        }
        return $this->setError($res);
    }
}
