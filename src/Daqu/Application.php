<?php

namespace JyPrint\Daqu;

use JyPrint\Kernel\Response;
use JyUtils\Str\Str;

/**
 * 大趋
 */
class Application extends Response
{
    use BarCode;
    
    private $config;
    private $requestUrl = 'https://printer.juhesaas.com';
    protected $printName = '大趋云';
    protected $errorNum = [
        '1419'  => '打印请求过快',
        '1420'  => '设备缓存队列发生溢出',
        '31405' => '消息发送失败，未知原因',
        '31406' => '消息发送失败，设备掉线',
        '31407' => '消息发送失败，接收超时',
        '31408' => '打印机参数列表为空',
        '31409' => '打印机SN号超过最大500个的限制',
        '31410' => '添加的SN号有重复',
        '31411' => '设备已绑定其他商户',
        '31412' => '设备已绑定',
        '31413' => '设备密钥错误',
        '31414' => '无效设备SN编码',
        '31415' => '打印请求记录不存在',
        '31416' => '音量参数无效',
        '31417' => '浓度参数无效',
        '31418' => '设备未绑定',
        '31419' => '无效的打印速度参数',
    ];
    
    public function __construct(array $config = [])
    {
        $this->config = $config;
        $this->setError('');
    }
    
    /**
     * 添加打印机
     *
     * @param array $prints 打印机列表
     * @return bool
     */
    public function add($prints)
    {
        $prints = $this->onlyfields($prints, ['sn', 'key', 'name', 'lang']);
        $res    = $this->http($this->requestUrl . "/openapi/addPrinter", [$prints]);
        if ($res['code'] == 0 || $res['code'] == 31412) {
            return true;
        }
        return $this->setError($res);
    }
    
    
    /**
     * 删除打印机
     *
     * @param array|string $sn 打印机编号
     */
    public function del($sn)
    {
        $prints = is_array($sn) ? $sn : ((array)$sn);
        $res    = $this->http($this->requestUrl . "/openapi/delPrinter", $prints);
        if ($res['code'] == 0) {
            return true;
        }
        return $this->setError($res);
    }
    
    /**
     * 获取打印机状态
     * https://www.yuque.com/docs/share/3138de29-7cd0-44d5-bede-c7ccf8c1c811#6574e4f329bcc1ce18a1f7a98feb5a5e
     * - onlineStatus      设备联网状态: 0 当前离线 1 在线
     * - workStatus        设备工作状态: 0 就绪, 1 打印中, 2 缺纸, 3 过温, 4 打印故障
     * - workStatusDesc    设备工作状态说明
     */
    public function getStatus($param)
    {
        $param = $this->onlyfields($param, ['sn']);
        $res   = $this->http($this->requestUrl . "/openapi/getDeviceStatus", $param);
        if ($res['code'] == 0) {
            $res['data']['status'] = $res['data']['onlineStatus'];
            return $res['data'];
        }
        return $this->setError($res);
    }
    
    /**
     * 打印小票
     * https://www.yuque.com/docs/share/3138de29-7cd0-44d5-bede-c7ccf8c1c811#191fca168ed125f0c43a21b3958f46dd
     */
    public function printTicket($param)
    {
        $param['times'] = $param['number'] ?: 1;
        unset($param['copies']);
        
        // 音源 https://www.yuque.com/docs/share/3138de29-7cd0-44d5-bede-c7ccf8c1c811#17839e73fc3f438b8f451642d98b0bab
        if (!$param['voice']) {
            $param['voice'] = 4;
        }
        
        $res = $this->http($this->requestUrl . "/openapi/print", $param);
        if ($res['code'] == 0) {
            return $res['data']['printId'];
        }
        return $this->setError($res);
    }
    
    
    /**
     * 获取Header通用参数
     *
     * @param array $data 参数
     * @return array
     */
    private function getHeader($data)
    {
        $uuid     = Str::uuid('');
        $time     = time();
        $json     = json_encode($data);
        $str      = $uuid . $this->config['appid'] . $time . $this->config['secret'] . $json;
        $header   = [];
        $header[] = "Content-Type:application/json";
        $header[] = 'appid:' . $this->config['appid'];
        $header[] = 'uid:' . $uuid;
        $header[] = 'stime:' . $time;
        $header[] = 'sign:' . md5($str);
        return $header;
    }
    
    /**
     * http 请求
     *
     * @param $url
     * @param $postfields
     * @param $method
     * @return array
     */
    private function http($url, $postfields = null, $method = 'POST')
    {
        $headers = $this->getHeader($postfields);
        $body    = json_encode($postfields);
        $ci      = curl_init();
        curl_setopt($ci, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ci, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ci, CURLOPT_TIMEOUT, 30);
        curl_setopt($ci, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ci, CURLINFO_HEADER_OUT, true);
        curl_setopt($ci, CURLOPT_HTTPHEADER, $headers); // 设置通用传参
        switch ($method) {
            case 'POST':
                curl_setopt($ci, CURLOPT_POST, true);
                if (!empty($postfields)) {
                    curl_setopt($ci, CURLOPT_POSTFIELDS, $body);
                }
                break;
        }
        
        curl_setopt($ci, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        curl_setopt($ci, CURLOPT_SSL_VERIFYHOST, false); // 从证书中检查SSL加密算法是否存在
        curl_setopt($ci, CURLOPT_URL, $url);
        $response  = curl_exec($ci);
        $http_code = curl_getinfo($ci, CURLINFO_HTTP_CODE);
        curl_close($ci);
        if ($http_code == 200 && $temp = json_decode($response, true)) {
            return $temp;
        }
        return $response;
    }
}
