<?php

namespace JyPrint\Daqu;

use JyPrint\UnifyPrint\UnifyPrint;

/**
 * 小票
 */
trait BarCode
{
    /**
     * 初始化统一打印
     * https://docs.ijingyi.com/web/#/74/2169
     */
    public function initPrintContent($paper = [])
    {
        $config = [
            'owner'       => 'Daqu',
            'model'       => 'labelGroup',
            'labelName'   => false,
            'text'        => '<font# bolder=0 height=0 width=0>{value}</font#>',
            'br'          => '<BR>',
            'lineAndEach' => true, // 每个处理后面自动加上br(换行)
            'lineAndBr'   => false,
            'cut'         => '',
            'cashBox'     => '', // 钱箱/声光报警器
            'barCode1'    => '<C><BAR>{value}</BAR></C>',
            'barCode2'    => '<C><BAR>{value}</BAR></C>',
            'qrCode'      => '<C><QR>{value}</QR></C>',
            'left'        => [
                // <LEFT>content</LEFT>
                'type'  => 'labelGroup',
                'value' => 'LEFT',
            ],
            'center'      => [
                // <C>content</C>
                'type'  => 'labelGroup',
                'value' => 'C',
            ],
            'right'       => [
                // <RIGHT>content</RIGHT>
                'type'  => 'labelGroup',
                'value' => 'RIGHT',
            ],
            'font_h1'     => '<font# bolder=0 height=2 width=2>{value}</font#>',
            'font_h2'     => '<font# bolder=0 height=2 width=2>{value}</font#>',
            'font_height' => [
                'name'  => 'height',
                'value' => 2,
            ],
            'font_width'  => [
                'name'  => 'width',
                'value' => 2,
            ],
            'font_bold'   => [
                'name'  => 'bolder',
                'value' => 1,
            ],
            'font_big'    => [
                [
                    'name'  => 'height',
                    'value' => 2,
                ],
                [
                    'name'  => 'width',
                    'value' => 2,
                ],
            ],
            'leftRight'   => false, // 左右对齐
            'row2col'     => false, // 1行2列
            'row3col'     => false, // 1行3列
            'row4col'     => false, // 1行4列
            // 排序，从[外]到[里]的顺序，越外面，放在越前面
            'sort'        => [
            
            ],
        ];
        return new UnifyPrint($config, $paper);
    }
}
