<?php
// 佳博
// https://dev.poscom.cn/openapi?adddev

namespace JyPrint\Poscom;

use JyPrint\Kernel\Http;
use JyPrint\Kernel\Response;

/**
 * Class Application.
 */
class Application extends Response
{
    use BarCode;
    use BarCodeLabel;
    
    private $config;
    private $requestUrl = 'https://api.poscom.cn';
    protected $printName = '佳博云';
    protected $errorNum = [
        '-1' => 'IP 地址不允许。',
        '-2' => '关键参数为空或请求方式不对。',
        '-3' => '客户编码不正确。',
        '-4' => '安全校验码不正确。',
        '-5' => '设备已被绑定，请检查。',
        '-6' => '设备未注册。',
    ];
    
    public function __construct(array $config = [])
    {
        $this->config = $config;
        $this->setError('');
    }
    
    /**
     * 添加打印机
     */
    public function add($param)
    {
        $param['deviceID'] = $param['sn'];
        $param['devName']  = $param['name'];
        unset($param['sn'], $param['name']);
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->requestUrl . '/apisc/adddev', $params);
        if (!isset($res['code'])) {
            $this->setError($res);
            return false;
        } else if ($res['code'] == 1) {
            return true;
        } else if ($res['code'] == -5) {
            return true;
        } else if (isset($this->errorNum[$res['code']])) {
            $this->setError($this->errorNum[$res['code']]);
            return false;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 删除打印机
     */
    public function del($sn)
    {
        $params = $this->getCommonParam(['deviceID' => $sn]);
        $res    = Http::httpPost($this->requestUrl . '/apisc/deldev', $params);
        if ($res['code'] == 1) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 取打印机列表
     *
     * @return bool
     */
    public function getList()
    {
        $params = $this->getCommonParam([], 1);
        $res    = Http::httpPost($this->requestUrl . '/apisc/listDevice', $params);
        if ($res['code'] == 1) {
            return $res['deviceList'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 清空待打印队列
     */
    public function clear($sn)
    {
        $params = $this->getCommonParam(['deviceID' => $sn, 'all' => 1]);
        $res    = Http::httpPost($this->requestUrl . '/apisc/cancelPrint', $params);
        if ($res['code'] == 1) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 获取打印机状态
     * 0=离线，1=在线，2=缺纸
     * https://dev.poscom.cn/openapi?getStatus
     */
    public function getStatus($param)
    {
        $param  = $this->onlyfields($param, ['sn']);
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->requestUrl . '/apisc/getStatus', $params);
        if ($res['code'] == 1 && isset($res['statusList'])) {
            return $res['statusList'][0]['status'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 设置打印机音量
     *
     * @param array $param
     * @return bool|false
     */
    public function setVoice($param)
    {
        $param['deviceID'] = $param['sn'];
        unset($param['sn']);
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->requestUrl . '/apisc/sendVolume', $params);
        if ($res['code'] == 0) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 设置打印机语音类型
     *
     * @param array $param
     * @return bool|false
     */
    public function setVoiceType($param)
    {
        $param['deviceID'] = $param['sn'];
        unset($param['sn']);
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->requestUrl . '/apisc/setVoiceType', $params);
        if ($res['code'] == 0) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 打印小票
     */
    public function printTicket($param)
    {
        $param['mode']      = $param['mode'] ?: 2;
        $param['times']     = $param['number'] ?: 1;
        $param['deviceID']  = $param['sn'];
        $param['msgDetail'] = $param['content'];
        unset($param['sn'], $param['content'], $param['number']);
        $params = $this->getCommonParam($param, 2);
        $res    = Http::httpPost($this->requestUrl . '/apisc/sendMsg', $params);
        if ($res['code'] == 0) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 打印标签
     */
    public function printLabel($param)
    {
        $param['mode']      = $param['mode'] ?: 2;
        $param['times']     = $param['number'] ?: 1;
        $param['deviceID']  = $param['sn'];
        $param['msgDetail'] = $param['content'];
        unset($param['sn'], $param['content'], $param['number']);
        $params = $this->getCommonParam($param, 2);
        $res    = Http::httpPost($this->requestUrl . '/apisc/sendMsg', $params);
        if ($res['code'] == 0) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 取公共参数
     */
    private function getCommonParam($params = [], $signType = 0)
    {
        $time                 = time() . rand(100, 999);
        $data                 = array_merge([
            'memberCode' => $this->config['apicode'],
            'reqTime'    => $time,
        ], $params);
        $data['securityCode'] = $this->getSign($time, $signType, $params['deviceID']);
        return $data;
    }
    
    /**
     * 生成签名
     */
    private function getSign($time, $signType = 0, $deviceID = null)
    {
        // 查询打印机列表时
        if ($signType == 1) {
            return md5($this->config['apicode'] . $time . $this->config['secret']);
            
            // 发送打印数据时
        } else if ($signType == 2) {
            return md5($this->config['apicode'] . $deviceID . $time . $this->config['secret']);
            
        } else {
            return md5($this->config['apicode'] . $time . $this->config['secret'] . $deviceID);
        }
    }
}
