<?php

namespace JyPrint\Ushengyun;

use JyPrint\UnifyPrint\UnifyPrint;

trait BarCode
{
    /**
     * 初始化统一打印
     */
    public function initPrintContent($paper = [])
    {
        $config = [
            'owner'       => 'Ushengyun',
            'model'       => 'label',
            'labelName'   => false,
            'text'        => false,
            'br'          => "<RN>",
            'line'        => false,
            'audio'       => '<MC>4</MC>',
            'cut'         => false,
            'cashBox'     => '<PLUGIN>',
            // 钱箱/声光报警器
            'barCode1'    => '<C><BR>{value}</BR></C>',
            'qrCode'      => '<C><QR>{value}</QR></C>',
            'left'        => "{value}<RN>",
            'center'      => "<C>{value}</C>",
            'right'       => "<R>{value}</R>",
            'font_h1'     => '<S2>{value}</S2>',
            'font_h2'     => '<S1>{value}</S1>',
            'font_height' => '<H1>{value}</H1>',
            'font_width'  => '<W1>{value}</W1>',
            'font_bold'   => '<B1>{value}</B1>',
            'font_big'    => '<S1>{value}</S1>',
            'leftRight'   => false,
            // 左右对齐
            'row2col'     => '<TR><TD>{content1}</TD><TD>{content2}</TD></TR>',
            // 1行2列
            'row3col'     => '<TR><TD>{content1}</TD><TD>{content2}</TD><TD>{content3}</TD></TR>',
            // 1行3列
            'row4col'     => '<TR><TD>{content1}</TD><TD>{content2}</TD><TD>{content3}</TD><TD>{content4}</TD></TR>',
            // 1行4列
            // 排序，从[外]到[里]的顺序，越外面，放在越前面
            'sort'        => [
                'C',
                'R',
                'S1',
                'S2',
                'H1',
                'B1',
            ],
        ];
        return new UnifyPrint($config, $paper);
    }
}
