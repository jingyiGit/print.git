<?php
// 365云打印机
// https://docs.ijingyi.com/web/#/74/6502

namespace JyPrint\A365;

use JyPrint\Kernel\Http;
use JyPrint\Kernel\Response;

/**
 * Class Application.
 */
class Application extends Response
{
    use BarCode;
    
    private $config;
    private $requestUrl = 'http://open.printcenter.cn:8080/';
    protected $printName = '365';
    
    public function __construct(array $config = [])
    {
        $this->config = $config;
        $this->setError('');
    }
    
    /**
     * 获取打印机状态
     * 返回打印机状态信息。共三种：
     * 1、离线。
     * 2、在线，工作状态正常。
     * 3、在线，工作状态不正常。
     * 备注：异常一般是无纸，离线的判断是打印机与服务器失去联系超过2分钟。
     */
    public function getStatus($param)
    {
        $param = [
            'deviceNo' => $param['sn'],  // 打印机编号
            'key'      => $param['key'], // 打印密钥
        ];
        $res   = Http::httpPost($this->requestUrl . 'queryPrinterStatus', $param);
        if ($res['responseCode'] == 0) {
            if (strpos($res['msg'], '在线,纸张正常') !== false) {
                return 2;
            } else if (strpos($res['msg'], '在线,缺纸') !== false) {
                return 3;
            } else {
                return 1;
            }
        }
        $this->setError($res);
        return 0;
    }
    
    
    /**
     * 打印小票
     */
    public function printTicket($param)
    {
        $param = [
            'deviceNo'     => $param['sn'],              // 打印机编号
            'key'          => $param['key'],             // 打印密钥
            'times'        => $param['number'] ?: 1,     // 打印次数
            'printContent' => $param['content'],         // 小票内容
        ];
        $res   = Http::httpPost($this->requestUrl . 'addOrder', $param);
        if ($res['responseCode'] == 0) {
            return true;
        }
        $this->setError($res);
        return false;
    }
}
