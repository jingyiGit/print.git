<?php

namespace JyPrint\TemplatePrint;

trait Huizg
{
  /**
   * 针对慧掌柜订单，自动转换打印数据
   *
   * @param array $order      订单信息
   * @param array $extra_info 额外信息
   * @return array|false
   */
  public static function transformationHuizg($order, $extra_info = [])
  {
    return '未完善，请联系果子';
    $meal_number = isset($order['shops'][0]['meal_number']) ? $order['shops'][0]['meal_number'] : '';
    $remarks     = isset($order['shops'][0]['remarks']) ? $order['shops'][0]['remarks'] : '';
    $order_info  = [
      'orderId'                => $order['order_id'],
      'daySeq'                 => $meal_number,
      'Remark'                 => $remarks,
      'peopleNumber'           => 0,
      'order_time'             => self::handleTime($order['created_at']),
      'deliveryTime'           => '',
      
      // 价格相关
      'originalPrice'          => $order['amount'],         // 原价
      'total'                  => $order['total_price'],    // 总价(实际收入)
      
      // 收货信息
      'customerName'           => isset($order['name']) ? $order['name'] : '',
      'customerPhone'          => isset($order['phone']) ? $order['phone'] : '',
      'customerAddress'        => $order['address'],
      'customerAddressDetails' => '',
      
      // 骑手信息
      'riderName'              => isset($order['rider']['nick_name']) ? $order['rider']['nick_name'] : '',
      'riderPhone'             => isset($order['rider']['mobile']) ? $order['rider']['mobile'] : '',
    ];
    
    // 商品列表
    $good_list = [];
    foreach ($order['groups'] as $group) {
      $good_list[] = [
        'name'  => $group['name'],
        'type'  => $group['type'],
        'items' => self::eleHandleGood($group['items']),
      ];
    }
    return [
      'order_info' => $order_info,
      'good_list'  => $good_list,
      'extra_info' => array_merge(self::eleGetExtraInfo($order), $extra_info),
    ];
  }
}
