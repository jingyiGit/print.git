<?php

namespace JyPrint\TemplatePrint;

/**
 * 饿了么零售
 * 内容格式说明：http://docs.ijingyi.com/web/#/74/2910
 */
trait EleRetailTo
{
    /**
     * 针对饿了么零售订单，自动转换打印数据
     *
     * @param array $order      订单信息
     * @param array $extra_info 额外信息
     * @return array|false
     */
    public static function transformationEleRetail($order, $extra_info = [])
    {
        if (!self::isEleRetailOrder($order)) {
            return false;
        }
        $order_info = [
            'orderId'                => $order['order_id'],
            'daySeq'                 => $order['order']['order_index'] ?: '',
            'Remark'                 => $order['order']['remark'],
            'peopleNumber'           => $order['order']['meal_num'] ?: 0,
            'order_time'             => self::handleTime($order['order']['create_time']),
            'deliveryTime'           => self::handleTime($order['order']['send_time'] && $order['order']['send_immediately'] == 1 ? $order['order']['send_time'] : 0),
            'reserve_time'           => self::handleTime($order['order']['latest_send_time']),
            
            // 价格相关
            'originalPrice'          => round($order['order']['total_fee'] / 100, 2),// 原价
            'total'                  => round($order['order']['user_fee'] / 100, 2), // 总价(实际收入)
            
            // 收货信息
            'customerName'           => $order['user']['name'],
            'customerPhone'          => $order['user']['phone'],
            'customerAddress'        => $order['user']['address'],
            'customerAddressDetails' => isset($order['user']['accurate_address']) ? $order['user']['accurate_address'] : '',
            
            // 骑手信息
            'riderName'              => '',
            'riderPhone'             => '',
        ];
        
        // 商品列表
        $good_list = [];
        $count     = 0;
        foreach ($order['products'] as $group) {
            $count++;
            $good_list[] = [
                'name'  => $count . '号篮子',
                'type'  => 'normal',
                'items' => self::eleRetailHandleGood($group),
            ];
        }
        return [
            'order_info' => $order_info,
            'good_list'  => $good_list,
            'extra_info' => array_merge(self::eleRetailGetExtraInfo($order), $extra_info),
        ];
    }
    
    private static function eleRetailHandleGood($goods)
    {
        $list = [];
        foreach ($goods as $good) {
            $temp   = [
                'name'      => $good['product_name'],
                'quantity'  => $good['product_amount'],
                'price'     => round($good['product_price'] / 100, 2), // 商品单价，单位：分
                'total'     => round($good['product_fee'] / 100, 2),   // 商品总价，单位：分
                'specsInfo' => self::eleRetailHandleSpescInfo($good),
            ];
            $list[] = $temp;
        }
        return $list;
    }
    
    /**
     * 获取规格/品味
     *
     * @param $good
     * @return string
     */
    private static function eleRetailHandleSpescInfo($good)
    {
        $specs = [];
        if (isset($good['product_features']) && is_array($good['product_features'])) {
            foreach ($good['product_features'] as $v) {
                $specs[] = $v['option'];
            }
        }
        return self::handleSpescInfoContainSymbol(implode(',', $specs));
    }
    
    /**
     * 处理额外信息
     *
     * @param array $order 订单信息
     * @return array
     */
    private static function eleRetailGetExtraInfo($order)
    {
        $extra_info = [];
        // 处理虚拟号码
        if (isset($order['user']['phone']) && $order['user']['phone']) {
            $temp                          = explode(',', $order['user']['phone']);
            $phone                         = $temp[0];
            $extension                     = isset($temp[1]) ? (' 转 ' . $temp[1]) : '';
            $extra_info['virtualNumber1']  = "虚拟号码: {$phone}{$extension}";
            $extra_info['virtualDescribe'] = '【如需联系顾客，请在呼叫主号码听到提示音后输入分机号，即可拨通顾客隐私号。】';
        }
        
        // 商铺名称
        if (isset($order['shop']['name']) && $order['shop']['name']) {
            $extra_info['shop_name'] = $order['shop']['name'];
        }
        return $extra_info;
    }
}
