<?php
// 订单转换

namespace JyPrint\TemplatePrint;

trait OrderTo
{
    /**
     * 自动转换订单打印数据，支持【美团，饿了么】
     *
     * @param array $order      订单信息
     * @param array $extra_info 额外信息
     * @return array|false
     */
    public static function transformationAuto($order, $extra_info = [])
    {
        if (self::isMtOrder($order)) {
            return self::transformationMt($order, $extra_info);
        } elseif (self::isEleOrder($order)) {
            return self::transformationEle($order, $extra_info);
        } elseif (self::isEleRetailOrder($order)) {
            return self::transformationEleRetail($order, $extra_info);
        }
        return [];
    }
    
    /**
     * 是否美团订单
     *
     * @param array $order 订单数据
     * @return bool
     */
    public static function isMtOrder($order)
    {
        $must_field = ['caution', 'daySeq', 'detail', 'recipientAddress'];
        foreach ($must_field as $field) {
            if (!isset($order[$field])) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * 是否饿了么订单
     *
     * @param array $order 订单数据
     * @return bool
     */
    public static function isEleOrder($order)
    {
        $must_field = ['description', 'daySn', 'groups', 'deliveryPoiAddress'];
        foreach ($must_field as $field) {
            if (!isset($order[$field])) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * 是否饿了么零售
     *
     * @param array $order 订单数据
     * @return bool
     */
    public static function isEleRetailOrder($order)
    {
        $must_field = ['source', 'shop.baidu_shop_id', 'order.eleme_order_id'];
        foreach ($must_field as $field) {
            $temp = explode('.', $field);
            if (count($temp) == 2) {
                if (!isset($order[$temp[0]]) || !isset($order[$temp[0]][$temp[1]])) {
                    return false;
                }
            } else {
                if (!isset($order[$field])) {
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * 处理时间
     *
     * @param string|int $time
     * @return array|false|int|string|string[]
     */
    private static function handleTime($time)
    {
        if (strlen(trim($time)) == 10) {
            $time = date('Y-m-d H:i', $time);
        } else if (strlen(trim($time)) == 13 && is_numeric($time)) {
            $time = date('Y-m-d H:i', $time / 1000);
        } else if (strpos($time, 'T') !== false) {
            $time = str_replace('T', ' ', $time);
        }
        return $time;
    }
    
    /**
     * 处理规格/品味，给他前后加上括号
     *
     * @param string $specsInfo
     * @return string
     */
    private static function handleSpescInfoContainSymbol($specsInfo)
    {
        if (!$specsInfo) {
            return '';
        } else if (strpos($specsInfo, '[') === false && strpos($specsInfo, '【') === false) {
            return "【{$specsInfo}】";
        }
        return $specsInfo;
    }
}
