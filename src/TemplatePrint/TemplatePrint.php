<?php
// 模板化打印，用于创建打印数据

namespace JyPrint\TemplatePrint;

use JyPrint\JyPrint;

class TemplatePrint
{
    use Error;
    use MtTo;
    use EleTo;
    use EleRetailTo;
    use Yida;
    use Nadouda;
    use Huizg;
    use OrderTo;
    use printFormat;

    private static $printDriveType = [];
    private static $print_data = [];
    private static $print;
    private static $error;

    public static function init($printDriveType)
    {
        self::$printDriveType = $printDriveType;
    }

    /**
     * 打印
     *
     * @param array $print_data   要打印的数据
     * @param array $print_format 打印格式定义(如:商品列表的字体大小，收货人是否显示等等)
     */
    public static function createPrintContent($print_data, $print_format = [])
    {
        self::$print_data   = $print_data;
        self::$print_format = array_merge(self::$print_format, $print_format);

        if (!isset($print_data['extra_info'])) {
            self::setError('extra_info 不能为空');
            return false;
        } elseif (!isset($print_data['order_info'])) {
            self::setError('order_info 不能为空');
            return false;
        }

        // 初始化打印驱动
        if (!$app = self::initDrive()) {
            self::setError('未初始化驱动，请先调用 TemplatePrint::init(); 初始化驱动。');
            return false;
        }

        // 水獭掌柜，不能设置打印格式，直接形成打印内容返回
        if (self::$printDriveType == 'Stzg') {
            return self::$print = $app->initPrintContent($print_data);
        }

        self::$print = $app->initPrintContent(self::getPaperInfo());
        self::$print->setPrintFormat('lineAndBr', false);

        // 开始创建打印格式
        self::_header();           // 头部
        self::_orderInfo();        // 订单信息
        self::_remark();           // 备注

        // 商品列表
        if (isset($print_data['good_list'])) {
            self::_goods();
        }

        self::_totalPrice();       // 总金额
        self::_customer();         // 顾客信息
        self::_riderInfo();        // 骑手信息
        self::_barCode();          // 条形码
        self::_qrCode();           // 二维码
        self::_footer();           // 结尾

        // 返回创建好的打印数据
        return self::$print->getPrintContent();
    }

    /**
     * 取字张信息
     *
     * @return array
     */
    private static function getPaperInfo()
    {
        $paper_width = self::getPrintDataValue('extra_info.paper_width', '58');
        $character   = $paper_width == '58' ? 32 : 48;
        return [
            'character'   => $character,
            'paper_width' => $paper_width,
        ];
    }

    /**
     * 头部
     */
    private static function _header()
    {
        $type = self::getPrintDataValue('order_info.type', 'none');
        if ($type == 'none') {
            self::$print->audio();
        } else if ($type == 'refund') {
            self::$print->audio_refund();
        }

        self::$print->br();

        $daySeq = self::getPrintDataValue('order_info.daySeq', ''); // 订单序号，今天的第几单，如：15
        if ($daySeq and strpos($daySeq, '#') === false) {
            $daySeq = '#' . $daySeq;
        }

        $type = self::getPrintDataValue('extra_info.type');             // 类型，如：顾客联，商家联，后厨联

        // 标签组(如佳博)
        if (self::$print->printFormat['model'] == 'labelGroup') {
            self::$print->center($daySeq . $type, false, true, true);
            self::$print->splitLine();
        } else {
            self::$print->splitLine('-', $daySeq . $type, true);
        }

        // 顶部插槽
        if ($temp = self::getPrintDataValue('extra_info.slot_top')) {
            $slot_data = self::analysisSlot($temp, false, true, false);
            foreach ($slot_data as $v) {
                self::$print->center($v['text'], $v['bold'], $v['height'], $v['width']);
            }
        }

        // 顶部插槽1
        if ($temp = self::getPrintDataValue('extra_info.slot_top1')) {
            $slot_data = self::analysisSlot($temp, false, true, false);
            foreach ($slot_data as $v) {
                self::$print->center($v['text'], $v['bold'], $v['height'], $v['width']);
            }
        }

        // 商铺名称
        $shop_name = self::getPrintDataValue('extra_info.shop_name');
        if (is_array($shop_name)) {
            self::$print->center($shop_name['text'], $shop_name['bold'], $shop_name['height'], $shop_name['width']);
        } else {
            self::$print->center($shop_name, false, true, false);
        }

        // 顶部插槽2
        if ($temp = self::getPrintDataValue('extra_info.slot_top2')) {
            $slot_data = self::analysisSlot($temp, false, true, false);
            foreach ($slot_data as $v) {
                self::$print->center($v['text'], $v['bold'], $v['height'], $v['width']);
            }
        }

        self::$print->splitLine();
    }

    /**
     * 订单信息
     */
    private static function _orderInfo()
    {
        // 预约时间
        $reserveFormat = self::getDiyInfo('reserve', []);
        if ($reserveFormat['show'] && $temp = self::getPrintDataValue('order_info.reserve_time')) {
            self::$print->left('预约时间: ' . $temp, $reserveFormat['fontBold'], $reserveFormat['fontHeight'], $reserveFormat['fontWidth']);
        }

        // 桌台号
        $tableNumberFormat = self::getDiyInfo('tableNumber', []);
        if ($tableNumberFormat['show'] && $temp = self::getPrintDataValue('order_info.tableNumber')) {
            self::$print->left('桌台: ' . $temp, $tableNumberFormat['fontBold'], $tableNumberFormat['fontHeight']);
        }

        // 就餐人数
        $peopleNumberFormat = self::getDiyInfo('peopleNumber', []);
        if ($peopleNumberFormat['show'] && $temp = self::getPrintDataValue('order_info.peopleNumber')) {
            self::$print->left('人数: ' . $temp, $peopleNumberFormat['fontBold'], $peopleNumberFormat['fontHeight']);
        }

        if ($temp = self::getPrintDataValue('extra_info.cashier_user')) {
            self::$print->left('收 银 员: ' . $temp);
        }

        if ($temp = self::getPrintDataValue('order_info.order_time')) {
            self::$print->left('下单时间: ' . $temp);
        }

        if ($temp = self::getPrintDataValue('order_info.pickup_time')) {
            self::$print->left('取货时间: ' . $temp);
        }

        self::$print->left('订单编号: ' . self::getPrintDataValue('order_info.orderId'));
        self::$print->splitLine();
    }

    /**
     * 备注
     */
    private static function _remark()
    {
        if ($temp = self::getPrintDataValue('order_info.Remark', '')) {
            self::$print->left('备注: ' . $temp, false, true, true);
        }
    }

    /**
     * 商品列表
     */
    private static function _goods()
    {
        // 套餐信息(自定义信息)
        $goodListFormat  = self::getDiyInfo('goodList', []);
        $specsInfoFormat = self::getDiyInfo('specsInfo', []);

        self::$print->br();
        self::$print->splitLine('*', '商品列表');
        self::$print->row3col('商品名称', '数量', '小计', $goodListFormat);

        $goods = self::getPrintDataValue('good_list');
        foreach ($goods as $item) {
            // 如: 1号篮子
            if ($item['name'] && $item['items']) {
                self::$print->splitLine('-', $item['name']);
            }

            // 商品列表
            foreach ($item['items'] as $good) {
                $good['quantity'] = $good['quantity'] == 0 ? '×' : $good['quantity'];
                self::$print->row3col($good['name'], $good['quantity'], self::handlePrice($good['total']), $goodListFormat);

                // 规则口味
                if (isset($good['specsInfo']) && $good['specsInfo'] && $specsInfoFormat['show']) {
                    $good['specsInfo'] = (array)$good['specsInfo'];
                    foreach ($good['specsInfo'] as $_specsInfo) {
                        self::$print->left($_specsInfo, $specsInfoFormat['fontBold'], $specsInfoFormat['fontHeight'], $specsInfoFormat['fontWidth']);
                    }
                }

                // 套餐信息
                if (isset($good['packageInfo']) && $good['packageInfo']) {
                    foreach ($good['packageInfo'] as $package) {
                        self::$print->left($package, $specsInfoFormat['fontBold'], $specsInfoFormat['fontHeight'], $specsInfoFormat['fontWidth']);
                    }
                }
            }
        }
        self::$print->splitLine('*');
        self::$print->br();
    }

    /**
     * 合计/应收
     */
    private static function _totalPrice()
    {
        // 中部插槽
        if (($temp = self::getPrintDataValue('extra_info.slot_top3', '')) != '') {
            $slot_data = self::analysisSlot($temp, false, false, false);
            foreach ($slot_data as $v) {
                self::$print->center($v['text'], $v['bold'], $v['height'], $v['width']);
            }
            if ($slot_data) {
                self::$print->splitLine();
            }
        }

        if (($temp = self::getPrintDataValue('order_info.originalPrice', '0', true)) !== false) {
            self::$print->right('合计金额：' . $temp);
            self::$print->splitLine();
        }

        // 中部插槽
        if (($temp = self::getPrintDataValue('extra_info.slot_top4', '')) != '') {
            $slot_data = self::analysisSlot($temp, false, false, false);
            foreach ($slot_data as $v) {
                self::$print->center($v['text'], $v['bold'], $v['height'], $v['width']);
            }
            if ($slot_data) {
                self::$print->splitLine();
            }
        }

        if (($temp = self::getPrintDataValue('order_info.total', '0', true)) !== false) {
            self::$print->h2('实付:' . $temp, 'right');
            self::$print->splitLine();
        }

        // 中部插槽
        if ($temp = self::getPrintDataValue('extra_info.slot_center', '')) {
            $slot_data = self::analysisSlot($temp, false, true, false);
            foreach ($slot_data as $v) {
                self::$print->center($v['text'], $v['bold'], $v['height'], $v['width']);
            }
            if ($slot_data) {
                self::$print->splitLine();
            }
        }
    }

    /**
     * 条形码
     */
    private static function _barCode()
    {
        // 条形码(自定义信息)
        $barCode = self::getDiyInfo('barCode', []);

        if ($barCode['show'] && $temp = self::getPrintDataValue('extra_info.barCode')) {
            self::$print->br();

            // 格式：[{"t":"新奥尔良烤翅","v":"3287012110729"},{"t":"新奥尔良烤翅","v":"3287012110729"}]
            if (is_array($temp)) {
                foreach ($temp as $item) {
                    if ($item['v'] && is_numeric($item['v'])) {
                        if ($item['t']) {
                            self::$print->center($item['t']);
                        }
                        self::$print->barCode($item['v']);
                    }
                }
            } else {
                if ($temp == 'orderId') {
                    $temp = self::getPrintDataValue('order_info.orderId');
                }
                self::$print->barCode($temp);
            }
        }
    }

    /**
     * 二维码
     */
    private static function _qrCode()
    {
        $qrCode = self::getPrintDataValue('extra_info.qrCode');
        if ($qrCode) {
            self::$print->br();
            self::$print->splitLine('-');
        }

        // 二维码顶部文本
        if ($temp = self::getPrintDataValue('extra_info.qrCodeTopText')) {
            self::$print->center($temp, false, true);
            self::$print->br();
        }

        if ($qrCode) {
            if ($qrCode == 'orderId') {
                $qrCode = self::getPrintDataValue('order_info.orderId');
            }
            self::$print->qrCode($qrCode);
        }

        // 二维码底部文本
        if ($temp = self::getPrintDataValue('extra_info.qrCodeBottomText')) {
            self::$print->center($temp);
        }
    }

    /**
     * 顾客信息
     */
    private static function _customer()
    {
        // 收货信息(自定义信息)
        $customer = self::getDiyInfo('customer', []);

        // 用户姓名
        if ($customer['show'] && $temp = self::getPrintDataValue('order_info.customerName')) {
            self::$print->left($temp, $customer['fontBold'], $customer['fontHeight'], $customer['fontWidth']);
        }

        // 用户手机
        if ($customer['show'] && $temp = self::getPrintDataValue('order_info.customerPhone')) {
            // 中间4位打码
            if ($customer['phoneShowType'] == 1) {
                $temp = self::handlePhoneCode($temp);

                // 显示尾4位
            } else if ($customer['phoneShowType'] == 2 && strlen(trim($temp)) == 11) {
                $temp = substr($temp, -4);
            }

            if (strlen($temp) == 4) {
                $temp = '手机尾号：' . $temp;
            }

            // 中间加空格
            $temp = self::handlePhone($temp);

            self::$print->left($temp, $customer['fontBold'], $customer['fontHeight'], $customer['fontWidth']);
        }

        // 虚拟号码(自定义信息)
        $virtualNumber = self::getDiyInfo('virtualNumber', []);

        // 虚拟号码1
        if ($virtualNumber['show'] && $temp = self::getPrintDataValue('extra_info.virtualNumber1')) {
            self::$print->left($temp, $virtualNumber['fontBold'], $virtualNumber['fontHeight']);
        }

        // 虚拟号码2
        if ($virtualNumber['show'] && $temp = self::getPrintDataValue('extra_info.virtualNumber2')) {
            self::$print->left($temp, $virtualNumber['fontBold'], $virtualNumber['fontHeight']);
        }

        // 虚拟号码说明
        if ($virtualNumber['show'] && $temp = self::getPrintDataValue('extra_info.virtualDescribe')) {
            self::$print->left($temp);
        }

        // 虚拟号码(自定义信息)
        $address = self::getDiyInfo('address', []);

        // 用户收货地址
        if ($address['show'] && $temp = self::getPrintDataValue('order_info.customerAddress', '')) {
            self::$print->left(
                $temp . self::getPrintDataValue('order_info.customerAddressDetails', ''),
                $address['fontBold'],
                $address['fontHeight'],
                $address['fontWidth']
            );
        } else {
            // self::$print->left('为保护隐私，顾客地址已隐藏。您可登录商家端或骑手端查看。');
        }
    }

    /**
     * 处理骑手信息
     */
    private static function _riderInfo()
    {
        // 骑手信息(自定义信息)
        $rider = self::getDiyInfo('rider', []);

        $riderName  = self::getPrintDataValue('order_info.riderName', '');
        $riderPhone = self::getPrintDataValue('order_info.riderPhone', '');
        if ($rider['show'] && ($riderName || $riderPhone)) {
            self::$print->splitLine('-', '骑手信息');
            if ($riderName) {
                self::$print->left($riderName, $rider['fontBold'], $rider['fontHeight'], $rider['fontWidth']);
            }
            if ($riderPhone) {
                // 中间加空格
                $riderPhone = self::handlePhone($riderPhone);
                self::$print->left($riderPhone, $rider['fontBold'], $rider['fontHeight'], $rider['fontWidth']);
            }
        }
    }

    /**
     * 结尾
     */
    private static function _footer()
    {
        // 底部插槽
        if ($temp = self::getPrintDataValue('extra_info.slot_bottom', '')) {
            self::$print->splitLine();
            $slot_data = self::analysisSlot($temp, false, true, false);
            foreach ($slot_data as $v) {
                self::$print->center($v['text'], $v['bold'], $v['height'], $v['width']);
            }
        }

        // 底部插槽1
        if ($temp = self::getPrintDataValue('extra_info.slot_bottom1', '')) {
            if (self::getPrintDataValue('extra_info.slot_bottom', '') == '') {
                self::$print->splitLine();
            }
            $slot_data = self::analysisSlot($temp, false, true, false);
            foreach ($slot_data as $v) {
                self::$print->center($v['text'], $v['bold'], $v['height'], $v['width']);
            }
        }

        // 订单序号，如：----#15完----
        $daySeq = self::getPrintDataValue('order_info.daySeq', ''); // 订单序号，今天的第几单，如：15
        if ($daySeq) {
            if (strpos($daySeq, '#') === false) {
                $daySeq = '#' . $daySeq;
            }
            // 标签组(如佳博)
            if (self::$print->printFormat['model'] == 'labelGroup') {
                self::$print->splitLine();
                self::$print->center($daySeq, false, true, true);
            } else {
                self::$print->splitLine('-', $daySeq . '完', true);
            }
        }
        self::$print->br(2);
    }

    /**
     * 取打印数据里的字段
     *
     * @param string $key
     * @param string $default
     * @param bool   $isPrice
     * @return array|mixed|string
     */
    private static function getPrintDataValue($key, $default = false, $isPrice = false)
    {
        $list = explode('.', $key);
        $temp = self::$print_data;
        foreach ($list as $k) {
            if (isset($temp[$k])) {
                $temp = $temp[$k];
            } else {
                return $isPrice ? self::handlePrice($default) : $default;
            }
        }
        return $isPrice ? self::handlePrice($temp) : $temp;
    }

    /**
     * 处理金额，未带小数点的，自动加上
     *
     * @param string $price
     * @return mixed|string
     */
    private static function handlePrice($price)
    {
        if ($price && $price != 0 && strpos($price, '.') === false) {
            return $price . '.0';
        }
        return $price;
    }


    /**
     * 初始化打印驱动
     *
     * @return \JyPrint\Ushengyun\Application
     */
    private static function initDrive()
    {
        if (self::$printDriveType == 'Ushengyun') {
            return JyPrint::Ushengyun([]);

        } elseif (self::$printDriveType == 'Xpyun') {
            return JyPrint::Xpyun([]);

        } elseif (self::$printDriveType == 'Poscom') {
            return JyPrint::Poscom([]);

        } elseif (self::$printDriveType == 'Spyun') {
            return JyPrint::Spyun([]);

        } elseif (self::$printDriveType == 'Feieyun') {
            return JyPrint::Feieyun([]);

        } elseif (self::$printDriveType == 'Ylyun') {
            return JyPrint::Ylyun([]);

        } elseif (self::$printDriveType == 'Zhongwuyun') {
            return JyPrint::Zhongwuyun([]);

        } elseif (self::$printDriveType == 'Bjguntong') {
            return JyPrint::Bjguntong([]);

        } elseif (self::$printDriveType == 'Stzg') {
            return JyPrint::Stzg([]);

        } elseif (self::$printDriveType == 'Stzg2') {
            return JyPrint::Stzg2([]);

        } elseif (self::$printDriveType == 'Daqu') {
            return JyPrint::Daqu([]);

        } elseif (self::$printDriveType == 'A365') {
            return JyPrint::A365([]);
        }
        return false;
    }

    /**
     * 处理手机号，中间加空格
     *
     * @param string $value
     */
    private static function handlePhone($value, $division = ' ')
    {
        if (strlen(trim($value)) != 11 || substr($value, 0, 1) != '1') {
            return $value;
        }
        $value = substr_replace($value, $division, 3, 0);
        return substr_replace($value, $division, 8, 0);
    }

    /**
     * 处理手机号，中间4位打码
     *
     * @param string $value
     */
    private static function handlePhoneCode($value, $division = '*')
    {
        if (strlen(trim($value)) != 11 || substr($value, 0, 1) != '1') {
            return $value;
        }
        return substr_replace($value, str_pad('', 4, $division), 3, 4);
    }

    /**
     * 解析插槽
     *
     * @param array $slot_data 插槽数据
     * @param bool  $bold      未设置时，是否默认加粗
     * @param bool  $height    未设置时，是否默认加高
     * @param bool  $width     未设置时，是否默认加宽
     * @return array
     */
    private static function analysisSlot($slot_data, $bold = false, $height = false, $width = false)
    {
        $template = [
            'text'   => '',
            'bold'   => $bold,
            'height' => $height,
            'width'  => $width,
        ];
        if (!is_array($slot_data)) {
            $template['text'] = $slot_data;
            return [$template];
        }

        $data = [];
        foreach ($slot_data as $v) {
            $temp = $template;
            if (is_array($v)) {
                if (!isset($v['text'])) {
                    continue;
                }
                $temp['text'] = $v['text'];
                if (isset($v['bold'])) {
                    $temp['bold'] = $v['bold'] == 1;
                }
                if (isset($v['height'])) {
                    $temp['height'] = $v['height'] == 1;
                }
                if (isset($v['width'])) {
                    $temp['width'] = $v['width'] == 1;
                }
            } else {
                $temp['text'] = $v;
            }
            $data[] = $temp;
        }
        return $data;
    }
}
