<?php

namespace JyPrint\Stzg;

use JyPrint\Kernel\Http;
use JyPrint\Kernel\Response;

/**
 * 水獭云打印
 *
 * @remark 使用创建订单的方式打印
 * @remark 文档说明：http://docs.ijingyi.com/web/#/74/3809
 *
 * 文档地址：https://developer-guides.tryotter.com/api-reference/#operation/createOrder
 */
class Application extends Response
{
    use BarCode;
    use store;
    
    private $config;
    // 备用地址1 https://partners.cloudkitchens.com
    // 备用地址2 https://partners.baitime.cn
    // 测试环境 https://import-webhook-staging.csschina.com
    private $requestUrl = 'https://partners.shuitazhanggui.com';
    protected $printName = '水獭掌柜';
    private $access_token = '';
    
    public function __construct(array $config = [])
    {
        $this->config = $config;
        $this->setError('');
    }
    
    /**
     * 设置Token
     *
     * @param string $access_token
     * @return void
     */
    public function setAccessToken($access_token)
    {
        $this->access_token = $access_token;
    }
    
    /**
     * 打印小票
     *
     * @remark 调用此方法先，需先调用 setAccessToken() 函数
     */
    public function printTicket($param)
    {
        Http::setHeaders([
            'Authorization'    => 'Basic ' . $this->getToken2(),
            'X-Application-Id' => $this->config['client_id'],
            'X-Store-Id'       => $param['store_id'],
        ]);
        
        $res = Http::httpPostJson($this->requestUrl . '/v1/orders?scope=' . urlencode('ping orders.create'), $param['content']);
        if (Http::$statusCode == 200) {
            return $res;
        }
        // 错误处理
        if (isset($res['message'])) {
            if (stripos($res['message'], 'Account could not be found') !== false) {
                $res['msg'] = '找不到帐户，原因：当前帐号未绑定，请联系平台进入绑定！';
            } else {
                $res['msg'] = $res['message'];
            }
        }
        if (!$res) {
            $res = [
                'gz' => '返回是空的',
            ];
        }
        
        // 重写错误信息
        if (isset($res['msg']) && strpos($res['msg'], '找不到帐户，原因：当前帐号未绑定') !== false) {
            $res['msg'] = '账户未注册，绑定打印机需联系平台客服(水獭官方要求)';
        }
        
        $res['statusCode'] = Http::$statusCode;
        return $this->setError($res);
    }
    
    /**
     * 打印退款小票
     * https://developer-guides.tryotter.com/api-reference/#operation/uploadPastOrders
     *
     * @param array $param
     * @return void
     */
    public function printTicketRefund($param)
    {
        $order_id = $param['refund_order_id'];
        unset($param['refund_order_id']);
        $data = [
            'orderStatus' => 'CANCELED',
        ];
        Http::setHeaders([
            'Authorization'    => 'Basic ' . $this->getToken2(),
            'X-Application-Id' => $this->config['client_id'],
            'X-Store-Id'       => $param['store_id'],
        ]);
        $res = Http::httpPostJson($this->requestUrl . '/v1/orders/' . urlencode($order_id) . '/status?scope=' . urlencode('ping orders.update'), $data);
        if (Http::$statusCode == 200) {
            return $res;
        }
        // 错误处理
        if (isset($res['message'])) {
            if (stripos($res['message'], 'Account could not be found') !== false) {
                $res['msg'] = '找不到帐户，原因：当前帐号未绑定，请联系平台进入绑定！';
            } else {
                $res['msg'] = $res['message'];
            }
        }
        if (!$res) {
            $res = [
                'gz' => '返回是空的',
            ];
        }
        $res['statusCode'] = Http::$statusCode;
        return $this->setError($res);
    }
    
    /**
     * 取Token，模式1
     *
     * @param string $scope 权限范围，可空，默认为：orders.create
     * @return false|mixed
     */
    public function getToken($scope = 'orders.create')
    {
        $param = [
            'scope'         => 'ping ' . $scope,
            'grant_type'    => 'client_credentials',
            'client_id'     => $this->config['client_id'],
            'client_secret' => $this->config['secret'],
        ];
        Http::setHeaders([
            'X-Application-Id' => $this->config['client_id'],
        ]);
        $res = Http::httpPost($this->requestUrl . '/v1/auth/token', $param);
        if (Http::$statusCode == 200) {
            $this->setAccessToken($res['access_token']);
            return $res;
        }
        $res['statusCode'] = Http::$statusCode;
        return $this->setError($res);
    }
    
    public function getToken2()
    {
        return base64_encode($this->config['client_id'] . ':' . $this->config['key']);
    }
    
    /**
     * 取Token，模式2
     *
     * @return void
     */
    public function getBasicAuth($scope)
    {
        $param = [
            'scope'      => 'ping ' . $scope,
            'grant_type' => 'client_credentials',
        ];
        Http::setHeaders([
            'Authorization' => 'Basic ' . base64_encode($this->config['client_id'] . ':' . $this->config['secret']),
        ]);
        $res = Http::httpPost($this->requestUrl . '/v1/auth/token', $param);
        if (Http::$statusCode == 200) {
            return $res;
        }
        $res['statusCode'] = Http::$statusCode;
        return $this->setError($res);
    }
}
