<?php

namespace JyPrint\Stzg;

use JyUtils\Str\Str;
use JyPrint\Kernel\Http;

trait store
{
    public function completeStore($storeId, $access_token)
    {
        $header = [
            'Authorization'    => 'bearer ' . $access_token,
            'X-Application-Id' => $this->config['client_id'],
            'X-Event-Id'       => Str::uuid(),
        ];
        Http::setHeaders($header);
        $param = [
            'success' => true,
            'storeId' => $storeId,
        ];
        $res   = Http::httpPostJson($this->requestUrl . '/v1/stores', $param);
        if (Http::$statusCode == 200) {
            return $res;
        }
        dd($this->requestUrl . '/v1/stores', $header, $param, $res, Http::$statusCode);
    }
    
    /**
     * https://developer-guides.tryotter.com/api-reference/#operation/upsertStorelinkWebhook
     * @param $storeId
     * @param $access_token
     * @return false|mixed
     */
    public function upsertStore($storeId, $access_token)
    {
        $header = [
            'Authorization'    => 'bearer ' . $access_token,
            'X-Application-Id' => $this->config['client_id'],
            'X-Event-Id'       => Str::uuid(),
        ];
        Http::setHeaders($header);
        $param = [
            'eventId'   => Str::uuid(),
            'eventTime' => date('Y-m-d') . 'T' . date('H:i:s') . '+01:00',
            'eventType' => 'stores.upsert',
            'metadata'  => [
                'storeId'       => $storeId,
                'applicationId' => $this->config['client_id'],
                'resourceId'    => '',
                'payload'       => [
                    'credentialsSchemaVersion' => '1.0',
                    'credentials'              => [
                        [
                            'email' => '',
                            'value' => '',
                        ],
                    ],
                    'storeInfo'                => [
                        'name'            => 'myName',
                        'address'         => 'Some Street, 1234',
                        'currencyCode'    => 'CNY',
                        'timezone'        => 'America/Los_Angeles',
                        'internalStoreId' => $storeId,
                    ],
                ],
                'resourceHref'  => '',
            ],
        ];
        $res   = Http::httpPostJson($this->requestUrl . '/v1/stores', $param);
        if (Http::$statusCode == 200) {
            return $res;
        }
        dd($this->requestUrl . '/v1/stores', $header, $param, $res, Http::$statusCode);
        
        if (!is_array($res)) {
            $res = ['error' => $res];
        }
        // 错误处理
        if (isset($res['message'])) {
            if (stripos($res['message'], 'Account could not be found') !== false) {
                $res['message'] = '找不以帐户，原因：当前帐号未绑定，请联系平台进入绑定！';
            }
        }
        $res['statusCode'] = Http::$statusCode;
        return $this->setError($res);
    }
    
    /**
     * https://developer-guides.tryotter.com/api-reference/#operation/fetchCredentialsWebhook
     * @return void
     */
    public function fetchStoreCreds($storeId, $access_token)
    {
        $header = [
            'Authorization'    => 'bearer ' . $access_token,
            'X-Application-Id' => $this->config['client_id'],
            'X-Event-Id'       => Str::uuid(),
        ];
        Http::setHeaders($header);
        $eventTime = date('Y-m-d') . 'T' . date('H:i:s') . '+01:00';
        $param     = [
            'eventId'   => Str::uuid(),
            'eventTime' => $eventTime,
            'eventType' => 'stores.fetch_credentials',
            'metadata'  => [
                'applicationId' => $this->config['client_id'],
                'storeId'       => $storeId,
            ],
        ];
        $res       = Http::httpPostJson($this->requestUrl . '/v1/stores', $param);
        if (Http::$statusCode == 200) {
            return $res;
        }
        dd($this->requestUrl . '/v1/stores', $header, $param, $res, Http::$statusCode);
        
        if (!is_array($res)) {
            $res = ['error' => $res];
        }
        $res['statusCode'] = strval(Http::$statusCode);
        return $this->setError($res);
    }
}
