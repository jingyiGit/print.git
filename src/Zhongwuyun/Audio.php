<?php

namespace JyPrint\Zhongwuyun;

trait Audio
{
  /**
   * Ai语音播报
   *
   * @param int $type 语音的标志, 目前支持：
   *                  4:您有新的订单,5:您有新的美团外卖订单,6:您有新的饿了么外卖订单，请注意查收,7:您有客户退单，请及时处理（实际效果以打印机播报为准）
   * @return string
   */
  public function audio($type)
  {
    return "<MC>{$type}</MC>";
  }
}
