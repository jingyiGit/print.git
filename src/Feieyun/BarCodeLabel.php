<?php

namespace JyPrint\Feieyun;

use JyPrint\UnifyPrintLabel\UnifyPrintLabel;

/**
 * 标签
 */
trait BarCodeLabel
{
    public function initPrintContentLabel()
    {
        $config = [
            'owner'       => 'Feieyun',
            'scale'       => 8,    // 比例，如：1mm=8dots，则填8
            'row_spacing' => 5,    // 行间距
            'margin'      => 10,   // 边距
            
            'normal_font_width'  => 24,    // 正常的字体宽度
            'normal_font_height' => 24,    // 正常的字体高度
            
            'h1_font_width'           => 48,        // 加宽的字体宽度
            'h1_font_height'          => 48,        // 加高的字体高度
            
            // 基础文本
            'basics_text'             => '<TEXT x="{x}" y="{y}" font="12" w="{w}" h="{h}" r="{r}">{value}</TEXT>',
            
            // 设置打印机设备内部的纸张大小
            'set_paper_size'          => '<SIZE>{width},{height}</SIZE>',
            
            // 打印方向，正向出纸
            'print_direction_forward' => '<DIRECTION>1</DIRECTION>',
            
            // 打印方向，反向出纸
            'print_direction_reverse' => '<DIRECTION>0</DIRECTION>',
            
            // 最后的附加文本
            'after'                   => '',
        ];
        return new UnifyPrintLabel($config);
    }
}
