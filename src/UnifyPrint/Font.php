<?php

namespace JyPrint\UnifyPrint;

trait Font
{
    /**
     * 字体加高
     */
    public function font_height($value)
    {
        if ($this->printModel == 'label') {
            return str_replace('{value}', $value, $this->printFormat['font_height']);

        } else if ($this->printModel == 'labelGroup') {
            return $this->handleFontLabelGroup($value, 'font_height');

        } else {
            return $value;
        }
    }

    /**
     * 字体加宽
     */
    private function font_width($value)
    {
        if ($this->printModel == 'label') {
            return str_replace('{value}', $value, $this->printFormat['font_width']);

        } else if ($this->printModel == 'labelGroup') {
            return $this->handleFontLabelGroup($value, 'font_width');

        } else {
            return $value;
        }
    }

    /**
     * 字体加粗
     */
    private function font_bold($value)
    {
        if ($this->printModel == 'label') {
            return str_replace('{value}', $value, $this->printFormat['font_bold']);

        } else if ($this->printModel == 'labelGroup') {
            return $this->handleFontLabelGroup($value, 'font_bold');

        } else {
            return $value;
        }
    }

    /**
     * 字体加大
     */
    private function font_big($value)
    {
        if ($this->printModel == 'label') {
            return str_replace('{value}', $value, $this->printFormat['font_big']);

        } else if ($this->printModel == 'labelGroup') {
            return $this->handleFontLabelGroup($value, 'font_big');

        } else {
            return $value;
        }
    }

    /**
     * 处理[标签组]格式的模式
     *
     * @param string $value 源码
     * @param string $field 属性字段
     * @return array|string|string[]|null
     */
    private function handleFontLabelGroup($value, $field)
    {
        if (!is_array($this->printFormat[$field])) {
            return $value;
        }
        if (isset($this->printFormat[$field]['type']) && $this->printFormat[$field]['type'] == 'labelGroup') {
            $label_name = $this->printFormat[$field]['value'];
            return "<{$label_name}>" . $value . "</{$label_name}>";

        } else if ($this->depth($this->printFormat[$field]) == 1) {
            return $this->handleFontLabelGroup1($value, $this->printFormat[$field]['name'], $this->printFormat[$field]['value']);
        } else {
            foreach ($this->printFormat[$field] as $v) {
                $value = $this->handleFontLabelGroup1($value, $v['name'], $v['value']);
            }
            return $value;
        }
    }

    private function handleFontLabelGroup1($value, $attName, $attValue)
    {
        $isHas = strpos($value, $attName) !== false;
        if ($isHas) {
            $pattern = "/({$attName}=)(\")?(\w+)(\")?/";
            return preg_replace($pattern, '${1}${2}' . $attValue . '${4}', $value);
        } else {
            $quotation = strpos($value, '"') !== false ? '"' : '';
            $pattern   = "/(<.*?)(>)/";
            return preg_replace($pattern, '${1} ' . $attName . '=' . $quotation . $attValue . $quotation . '${2}', $value, 1);
        }
    }
}
