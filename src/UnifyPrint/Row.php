<?php

namespace JyPrint\UnifyPrint;

trait Row
{
    /**
     * 左右排版对齐
     *
     * @param string $str_left   左边字符串
     * @param string $str_right  右边字符串
     * @param array  $fontFormat 字体格式，fontBold(加粗)，fontHeight(加高)，fontWidth(加宽)
     * @return void
     */
    public function leftRight($str_left, $str_right, $fontFormat = [])
    {
        if ($this->printFormat['leftRight']) {
            $temp               = str_replace('{left}', $str_left, $this->printFormat['leftRight']);
            $temp               = str_replace('{right}', $str_right, $temp);
            $this->printContent .= $temp;
        } else {
            $this->row2col($str_left, $str_right, true, $fontFormat);
        }
    }
    
    /**
     * 一行2列
     *
     * @param string $content1       第1列内容
     * @param string $content2       第2列内容
     * @param bool   $leftRightAlign 是否两端对齐
     * @param array  $fontFormat     字体格式，fontBold(加粗)，fontHeight(加高)，fontWidth(加宽)
     * @return void
     */
    public function row2col($content1, $content2, $leftRightAlign = false, $fontFormat = [])
    {
        // 1行2行，非两端对齐
        if ($this->printFormat['row2col'] && !$leftRightAlign && !$fontFormat) {
            $temp               = str_replace('{content1}', $content1, $this->printFormat['row2col']);
            $temp               = str_replace('{content2}', $content2, $temp);
            $this->printContent .= $temp;
            
            // 两端对齐
        } else {
            $length = $this->paperFormat['character'] ?? 32;
            [$list1, $list2] = $this->divisionLine2col($content1, $content2, $rows);
            
            // 处理字体格式
            $list1 = $this->rowHandleFontFormat($list1, $fontFormat);
            $list2 = $this->rowHandleFontFormat($list2, $fontFormat);
            
            for ($i = 0; $i < $rows; $i++) {
                $temp_length1 = $length;
                $temp_length2 = $length;
                
                $temp1 = isset($list1[$i]) ? $list1[$i] : '';
                // 字段是否加大
                if ($this->isBig($temp1)) {
                    $temp_length1 = $temp_length1 / 2;
                }
                
                $temp2 = isset($list2[$i]) ? $list2[$i] : '';
                // 字段是否加大
                if ($this->isBig($temp2)) {
                    $temp_length2 = $temp_length2 / 2;
                }
                
                $this->text(
                    $this->str_pad($temp1, $temp_length1 / 2 + 2, ' ') . '  ' .
                    $this->str_pad($temp2, $temp_length2 / 2 - 4, ' ', ($leftRightAlign ? STR_PAD_LEFT : STR_PAD_RIGHT))
                );
            }
        }
    }
    
    /**
     * 一行3列
     *
     * @param string $content1   第1列内容
     * @param string $content2   第2列内容
     * @param string $content3   第3列内容
     * @param array  $fontFormat 字体格式，fontBold(加粗)，fontHeight(加高)，fontWidth(加宽)
     * @return void
     */
    public function row3col($content1, $content2, $content3, $fontFormat = [])
    {
        if ($this->printFormat['row3col'] && !$fontFormat) {
            $temp               = str_replace('{content1}', $content1, $this->printFormat['row3col']);
            $temp               = str_replace('{content2}', $content2, $temp);
            $temp               = str_replace('{content3}', $content3, $temp);
            $this->printContent .= $temp;
        } else {
            $length = $this->paperFormat['character'] ?? 32;
            [$list1, $list2, $list3] = $this->divisionLine3col($content1, $content2, $content3, $rows);
            
            // 处理字体格式
            $list1 = $this->rowHandleFontFormat($list1, $fontFormat);
            $list2 = $this->rowHandleFontFormat($list2, $fontFormat);
            $list3 = $this->rowHandleFontFormat($list3, $fontFormat);
            
            for ($i = 0; $i < $rows; $i++) {
                $temp1 = isset($list1[$i]) ? $list1[$i] : '';
                $temp2 = isset($list2[$i]) ? $list2[$i] : '';
                $temp3 = isset($list3[$i]) ? $list3[$i] : '';
                
                // 大趋的要独立兼容
                if ($this->printFormat['owner'] == 'Daqu') {
                    $this->text(
                        $this->str_pad($temp1, $length - 14, ' ') .
                        $this->str_pad($temp2, 6, ' ', STR_PAD_LEFT) .
                        $this->str_pad($temp3, 8, ' ', STR_PAD_LEFT),
                        $fontFormat['fontBold'],
                        $fontFormat['fontHeight'],
                        $fontFormat['fontWidth']
                    );
                } else {
                    $this->text(
                        $this->str_pad($temp1, $length - 14, ' ') .
                        $this->str_pad($temp2, 6, ' ', STR_PAD_LEFT) .
                        $this->str_pad($temp3, 8, ' ', STR_PAD_LEFT)
                    );
                }
            }
        }
    }
    
    /**
     * 一行4列
     *
     * @param string $content1   第1列内容
     * @param string $content2   第1列内容
     * @param string $content3   第1列内容
     * @param string $content4   第1列内容
     * @param array  $fontFormat 字体格式，fontBold(加粗)，fontHeight(加高)，fontWidth(加宽)
     * @return void
     */
    public function row4col($content1, $content2, $content3, $content4, $fontFormat = [])
    {
        if ($this->printFormat['row4col'] && !$fontFormat) {
            $temp               = str_replace('{content1}', $content1, $this->printFormat['row4col']);
            $temp               = str_replace('{content2}', $content2, $temp);
            $temp               = str_replace('{content3}', $content3, $temp);
            $temp               = str_replace('{content4}', $content4, $temp);
            $this->printContent .= $temp;
        } else {
            $length = $this->paperFormat['character'] ?? 32;
            [
                $list1,
                $list2,
                $list3,
                $list4,
            ] = $this->divisionLine4col($content1, $content2, $content3, $content4, $rows);
            
            // 处理字体格式
            $list1 = $this->rowHandleFontFormat($list1, $fontFormat);
            $list2 = $this->rowHandleFontFormat($list2, $fontFormat);
            $list3 = $this->rowHandleFontFormat($list3, $fontFormat);
            $list4 = $this->rowHandleFontFormat($list4, $fontFormat);
            
            for ($i = 0; $i < $rows; $i++) {
                $temp1 = isset($list1[$i]) ? $list1[$i] : '';
                $temp2 = isset($list2[$i]) ? $list2[$i] : '';
                $temp3 = isset($list3[$i]) ? $list3[$i] : '';
                $temp4 = isset($list4[$i]) ? $list4[$i] : '';
                
                $this->text(
                    $this->str_pad($temp1, $length - 19, ' ') .
                    $this->str_pad($temp2, 6, ' ', STR_PAD_LEFT) .
                    $this->str_pad($temp3, 6, ' ', STR_PAD_LEFT) .
                    $this->str_pad($temp4, 7, ' ', STR_PAD_LEFT)
                );
            }
        }
    }
    
    /**
     * 处理 1行2列3列4列的字体格式
     *
     * @param string $content    要处理的内容
     * @param array  $fontFormat 字体格式，fontBold(加粗)，fontHeight(加高)，fontWidth(加宽)
     */
    private function rowHandleFontFormat($content, $fontFormat)
    {
        if (!$fontFormat) {
            return $content;
        }
        // 不让加宽
        if (isset($fontFormat['fontWidth'])) {
            unset($fontFormat['fontWidth']);
        }
        
        if (isset($fontFormat['fontBold']) && $fontFormat['fontBold']) {
            if (is_array($content)) {
                foreach ($content as &$v) {
                    $v = $this->font_bold($v);
                }
            } else {
                $content = $this->font_bold($content);
            }
        }
        
        if (isset($fontFormat['fontHeight']) && $fontFormat['fontHeight'] && isset($fontFormat['fontWidth']) && $fontFormat['fontWidth']) {
            if (is_array($content)) {
                foreach ($content as &$v) {
                    $v = $this->font_big($v);
                }
            } else {
                $content = $this->font_big($content);
            }
            
        } else {
            if (isset($fontFormat['fontHeight']) && $fontFormat['fontHeight']) {
                if (is_array($content)) {
                    foreach ($content as &$v) {
                        $v = $this->font_height($v);
                    }
                } else {
                    $content = $this->font_height($content);
                }
            }
            
            if (isset($fontFormat['fontWidth']) && $fontFormat['fontWidth']) {
                if (is_array($content)) {
                    foreach ($content as &$v) {
                        $v = $this->font_width($v);
                    }
                } else {
                    $content = $this->font_width($content);
                }
            }
        }
        return $content;
    }
    
    private function str_pad($string, $length, $str = ' ', $pad_type = STR_PAD_RIGHT)
    {
        // 取出一段文本中，标签的字符长度，如：<H1>商品名称</H1>，标签的字符长度为9
        $tagsLength = $this->getTagsLength($string);
        
        // 取出汉字的数量，加上
        $cLength = $this->getChineseCount(strip_tags($string));
        return str_pad($string, $tagsLength + $length + $cLength, $str, $pad_type);
    }
    
    /**
     * 分割行，2列
     *
     * @param string $content1
     * @param string $content2
     */
    public function divisionLine2col($content1, $content2, &$count)
    {
        $length = $this->paperFormat['character'] ?? 32;
        $list1  = $this->divisionLine($content1, $length / 2 + 2);
        $list2  = $this->divisionLine($content2, $length / 2 - 4);
        if (count($list1) > count($list2)) {
            $count = count($list1);
        } else {
            $count = count($list2);
        }
        return [$list1, $list2];
    }
    
    /**
     * 分割行，3列
     *
     * @param string $content1
     * @param string $content2
     */
    public function divisionLine3col($content1, $content2, $content3, &$count)
    {
        $length = $this->paperFormat['character'] ?? 32;
        $list1  = $this->divisionLine($content1, $length - 14);
        $list2  = $this->divisionLine($content2, 6);
        $list3  = $this->divisionLine($content3, 8);
        $count  = count($list1);
        return [$list1, $list2, $list3];
    }
    
    /**
     * 分割行，4列
     *
     * @param string $content1
     * @param string $content2
     */
    public function divisionLine4col($content1, $content2, $content3, $content4, &$count)
    {
        $length = $this->paperFormat['character'] ?? 32;
        $list1  = $this->divisionLine($content1, $length - 19);
        $list2  = $this->divisionLine($content2, 6);
        $list3  = $this->divisionLine($content3, 6);
        $list4  = $this->divisionLine($content4, 7);
        $count  = count($list1);
        return [$list1, $list2, $list3, $list4];
    }
    
    /**
     * 文本分行
     *
     * @param string $content 源文本
     * @param int    $length  每行的文本长度
     * @return array
     */
    private function divisionLine($content, $length)
    {
        $list  = [];
        $stop  = false;
        $count = 0;
        // 过滤标签组
        $temp_content = strip_tags($content);
        
        // 标签组，如：<L><B>{content}</B></L>
        $tag_group = str_replace($temp_content, '{content}', $content);
        
        while ($stop == false) {
            $temp = $this->cutstr($temp_content, $length);
            if ($tag_group) {
                $list[] = str_replace('{content}', $temp, $tag_group);
            } else {
                $list[] = $temp;
            }
            
            $temp_content = $this->str_replace_once($temp, '', $temp_content);
            $stop         = $temp_content == '';
            
            $count++;
            if ($count >= 5) {
                break;
            }
        }
        return $list;
    }
    
    /**
     * 内容是否有加大字体
     *
     * @param string $content 目标内容
     * @return bool
     */
    private function isBig($content)
    {
        if (!$content || !$this->printFormat['font_big']) {
            return false;
        } elseif ($this->printFormat['model'] != 'label') {
            return false;
        }
        
        // <B>{value}</B> 取出第一个标签
        if (!preg_match('#<\w+>#', $content, $res)) {
            return false;
        }
        
        if (count($res) > 0 && strpos($this->printFormat['font_big'], $res[0]) !== false) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * 取出一段文本中，标签的字符长度，如：<H1>商品名称</H1>，标签的字符长度为9
     *
     * @param string $value
     * @return int
     */
    private function getTagsLength($value)
    {
        if (!preg_match_all('#<.*?>#', $value, $res)) {
            return 0;
        }
        $length = 0;
        foreach ($res[0] as &$v) {
            $length += strlen($v);
        }
        return $length;
    }
    
    private function str_replace_once($needle, $replace, $haystack)
    {
        $pos = strpos($haystack, $needle);
        if ($pos === false) {
            //开源软件:phpfensi.com
            return $haystack;
        }
        return substr_replace($haystack, $replace, $pos, strlen($needle));
    }
}
